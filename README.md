# minidevuan

## Version 1 

little grub2 boot loader + DEVUAN amd64, running from /dev/sda2  (main disk, for diskless machines).
(6GB to host devuan on /dev/sda2)

Target: Idea Center, Lenovo, diskless for multimedia.

sda1:  
The grub2 is located on /dev/sda1. It is hosted by netbsd, amd64 with config in /grub/grub.cfg.
A single config line is : (hd0,msdos2)
The linux kernel is by default ascii devuan, 4.x with a root=/dev/sda2. 

sda2:
DEVUAN


The rootfs on /dev/sda2 can be copied from: 

https://gitlab.com/openbsd98324/devuan-live/-/raw/master/v1/ascii/amd64/DEVUAN.tar.gz



## Version 2 

ascii devuan, amd64 with nanogrub to sda2 (auto).
devuan.tar.gz is rootfs. 








